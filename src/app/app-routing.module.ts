import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PersonsComponent } from './components/persons/persons.component'
import { LoginComponent } from './components/login/login.component'
import { InformationComponent } from './components/information/information.component'

const routes: Routes = [
  {
    path: 'personas',
    component: PersonsComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'informacion/:idPerson',
    component: InformationComponent
  },
  {
    path: '**',
    redirectTo: '/personas'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})



export class AppRoutingModule { }
