import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

export class GlobalServiceService {

    loginValue:boolean = false;
    showSpinner:boolean = false;

    constructor() { }

}
