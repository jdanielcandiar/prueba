import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class GetInfoService {
    constructor(private http: HttpClient) { }

    dataPersons:any = undefined;
    person:any = undefined;

    /* Funciones GET */
    getInfoJsonGet(url:string){
        return this.http.get(url);
    }  

    getInfoPost(url:string, tokenBearer:string, bodyPost:any){

        let body = bodyPost;

        var header = {
            headers: new HttpHeaders()
              .set('Authorization',  tokenBearer)
        }
        return this.http.post(url, body, header);
    }

}
