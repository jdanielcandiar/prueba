import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router'
import { GetInfoService } from 'src/app/services/get-info.service';
import { GlobalServiceService } from 'src/app/services/global-service.service';


@Component({
  selector: 'app-information',
  templateUrl: './information.component.html',
  styleUrls: ['./information.component.scss']
})
export class InformationComponent implements OnInit {
    
    
    urlDataPersons:string = 'https://test-api-nebula.recaudia.com/person/search';
    tokenBearerPerson:string = 'Bearer TjUVdA1QOATIwiZ7Cp';
    bodyPost:any = {
        "conditions": {
        }
    }

    idPerson:any;
    infoPerson:any;

    constructor(public _getInfoService:GetInfoService,
                public _globalService:GlobalServiceService,
                private activatedRouter: ActivatedRoute,
                private router:Router) {
        
        this.activatedRouter.params.subscribe( params =>{
            this.idPerson = params.idPerson;
            this.getInfoPersons();
        })
    }

    ngOnInit(): void {
        
    }

    getInfoPersons(){

        
        this._globalService.showSpinner = true;
        this._getInfoService.getInfoPost(this.urlDataPersons,this.tokenBearerPerson,this.bodyPost)
        .subscribe( 
            res => {
                this._getInfoService.dataPersons = res;
                this._globalService.showSpinner = false;
                this.findPerson();
            },
            err => {
                console.log(err);
                this._globalService.showSpinner = false;
            }
        )
        
    }

    findPerson(){

        const found = this._getInfoService.dataPersons.results.find(x => x.person_id === this.idPerson);
        console.log(found)
        if(found !== undefined){
            this.infoPerson = found;
        }else{
            this.router.navigate(['/']);
        }

    }

}
