import { Component, OnInit } from '@angular/core';
import { GlobalServiceService } from 'src/app/services/global-service.service';
import { Router } from '@angular/router'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private _globalService : GlobalServiceService,
              private router:Router) { }

  ngOnInit(): void {
  }

  logIn(){
    this._globalService.loginValue = true;
    this.router.navigate(['/personas']);
  }

}
