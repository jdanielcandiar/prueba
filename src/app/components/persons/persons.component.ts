import { Component, OnInit } from '@angular/core';
import { GetInfoService } from 'src/app/services/get-info.service';
import { GlobalServiceService } from 'src/app/services/global-service.service';

@Component({
  selector: 'app-persons',
  templateUrl: './persons.component.html',
  styleUrls: ['./persons.component.scss']
})
export class PersonsComponent implements OnInit {

    p: number = 1;
    urlDataPersons:string = 'https://test-api-nebula.recaudia.com/person/search';
    tokenBearerPerson:string = 'Bearer TjUVdA1QOATIwiZ7Cp';
    bodyPost:any = {
        "conditions": {
        }
    }

    constructor(public _getInfoService:GetInfoService,
                public _globalService:GlobalServiceService) { }

    ngOnInit(): void {
        this.getInfoPersons();
    }
    
    getInfoPersons(){
        if(this._getInfoService.dataPersons === undefined){
            this._globalService.showSpinner = true;
            this._getInfoService.getInfoPost(this.urlDataPersons,this.tokenBearerPerson,this.bodyPost)
            .subscribe( 
                res => {
                    console.log(res)
                    this._getInfoService.dataPersons = res;
                    this._globalService.showSpinner = false;
                },
                err => {
                    console.log(err);
                    this._globalService.showSpinner = false;
                }
            )
        }
    }


}
