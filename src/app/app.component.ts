import { Component } from '@angular/core';
import { GlobalServiceService } from './services/global-service.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})


export class AppComponent {
    title = 'recaudia';

    constructor(public _globalService : GlobalServiceService){

    }
}
